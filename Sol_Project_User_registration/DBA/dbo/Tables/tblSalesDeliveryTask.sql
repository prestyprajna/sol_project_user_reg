﻿CREATE TABLE [dbo].[tblSalesDeliveryTask] (
    [SalesDeliveryTaskId] NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [EmployeeId]          NUMERIC (18) NOT NULL,
    [SalesTransactionId]  NUMERIC (18) NOT NULL,
    PRIMARY KEY CLUSTERED ([SalesDeliveryTaskId] ASC)
);

