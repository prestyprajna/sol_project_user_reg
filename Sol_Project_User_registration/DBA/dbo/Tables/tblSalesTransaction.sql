﻿CREATE TABLE [dbo].[tblSalesTransaction] (
    [SalesTransactionId]   NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [SalesTransactionDate] DATETIME     NULL,
    [ProductId]            NUMERIC (18) NULL,
    [TotalQuantity]        INT          NULL,
    [TotalPrice]           BIGINT       NULL,
    [PersonId]             INT          NULL,
    [Status]               VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([SalesTransactionId] ASC)
);

