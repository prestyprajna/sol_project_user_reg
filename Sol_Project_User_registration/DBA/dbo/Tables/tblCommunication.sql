﻿CREATE TABLE [dbo].[tblCommunication] (
    [PersonId] NUMERIC (18) NOT NULL,
    [MobileNo] VARCHAR (10) NULL,
    [EmailId]  VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

