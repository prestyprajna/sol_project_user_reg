﻿CREATE TABLE [dbo].[tblAddress] (
    [PersonId]   NUMERIC (18) NOT NULL,
    [HouseNo]    VARCHAR (50) NULL,
    [HouseName]  VARCHAR (50) NULL,
    [StreetName] VARCHAR (50) NULL,
    [AreaName]   VARCHAR (50) NULL,
    [CityName]   VARCHAR (50) NULL,
    [StateName]  VARCHAR (50) NULL,
    [Pincode]    BIGINT       NULL,
    PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

