﻿CREATE TABLE [dbo].[tblEmployee] (
    [EmployeeId] NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [PersonId]   NUMERIC (18) NOT NULL,
    PRIMARY KEY CLUSTERED ([EmployeeId] ASC)
);

