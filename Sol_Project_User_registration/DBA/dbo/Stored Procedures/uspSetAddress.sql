﻿CREATE PROCEDURE uspSetAddress
(
	@Command VARCHAR(50),
	@PersonId NUMERIC(18,0),	
	@HouseNo VARCHAR(50),
	@HouseName VARCHAR(50),
	@StreetName VARCHAR(50),
	@AreaName VARCHAR(50),
	@CityName VARCHAR(50),
	@StateName VARCHAR(50),
	@Pincode BIGINT	
)
AS 

	BEGIN

	--declaration
	DECLARE @ErrorMessage varchar(MAX)

		IF @Command='INSERT'
		BEGIN 

		BEGIN TRANSACTION
			BEGIN TRY 

				INSERT INTO tblAddress
				(
					PersonId,				
					HouseNo,
					HouseName,
					StreetName,
					AreaName,
					CityName,
					StateName,
					Pincode

				)
				VALUES
				(	
					@PersonId,			
					@HouseNo,
					@HouseName,
					@StreetName,
					@AreaName,
					@CityName,
					@StateName,
					@Pincode
				)
				COMMIT TRANSACTION
			END TRY 

			BEGIN CATCH 
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION	
				RAISERROR(@ErrorMessage,16,1)
			END CATCH	


		END


	END

