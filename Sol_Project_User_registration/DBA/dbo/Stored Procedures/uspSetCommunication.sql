﻿CREATE PROCEDURE uspSetCommunication
(
	@Command VARCHAR(50),
	@PersonId NUMERIC(18,0),	
	@MobileNo VARCHAR(10) ,
	@EmailId VARCHAR(50)
	  	 
)
AS 

	BEGIN

	--declaration
	DECLARE @ErrorMessage varchar(MAX)

		IF @Command='INSERT'
		BEGIN 

		BEGIN TRANSACTION
			BEGIN TRY 

				INSERT INTO tblCommunication
				(
					PersonId,
					MobileNo,
					EmailId				
				)
				VALUES
				(
					@PersonId,
					@MobileNo,
					@EmailId
				)
				COMMIT TRANSACTION
			END TRY 

			BEGIN CATCH 
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
			END CATCH	


		END


	END

