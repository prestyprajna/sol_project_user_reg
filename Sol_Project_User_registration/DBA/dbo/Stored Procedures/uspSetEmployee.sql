﻿CREATE PROCEDURE uspSetEmployee
(
	@Command VARCHAR(50),
	@EmployeeId NUMERIC(18,0),
	@PersonId NUMERIC(18,0),
	--@FirstName VARCHAR(50),
	--@LastName VARCHAR(50),
	--@Type VARCHAR(2),

	--@PersonId NUMERIC(18,0),
	@HouseNo VARCHAR(50),
	@HouseName VARCHAR(50),
	@StreetName VARCHAR(50),
	@AreaName VARCHAR(50),
	@CityName VARCHAR(50),
	@StateName VARCHAR(50),
	@Pincode BIGINT,

	--@PersonId NUMERIC(18,0),
	@MobileNo VARCHAR(10) ,
	@EmailId VARCHAR(50),
	
	@Username VARCHAR(50),
	@Password VARCHAR(50),
	@UseType VARCHAR(50)
)
AS 

	BEGIN

	--declaration
	DECLARE @ErrorMessage varchar(MAX)

		IF @Command='INSERT'
		BEGIN 

		BEGIN TRANSACTION
			BEGIN TRY 

				INSERT INTO tblEmployee
				(
					PersonId				
				)
				VALUES
				(
					@PersonId			
				)

				SET @PersonId=@@IDENTITY			

				--call address Sp
				EXEC uspSetAddress @Command,@PersonId,@HouseNo,@HouseName,@StreetName,
				@AreaName,@CityName,@StateName,@Pincode

				--call communication Sp
				EXEC uspSetCommunication @Command,@PersonId,@MobileNo,@EmailId

				--call Login Sp
				EXEC uspSetLogin @Command,@PersonId,@Username,@Password,@UseType

				COMMIT TRANSACTION

			END TRY 

			BEGIN CATCH 
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
			END CATCH	

		END


	END

