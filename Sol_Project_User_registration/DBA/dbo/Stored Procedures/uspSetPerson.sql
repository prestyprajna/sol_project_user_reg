﻿CREATE PROCEDURE uspSetPerson
(
	@Command VARCHAR(50),
	@PersonId NUMERIC(18,0),

	@CustomerId NUMERIC(18,0),
	@EmployeeId NUMERIC(18,0),

	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Type VARCHAR(2),

	--@PersonId NUMERIC(18,0),
	@HouseNo VARCHAR(50),
	@HouseName VARCHAR(50),
	@StreetName VARCHAR(50),
	@AreaName VARCHAR(50),
	@CityName VARCHAR(50),
	@StateName VARCHAR(50),
	@Pincode BIGINT,

	--@PersonId NUMERIC(18,0),
	@MobileNo VARCHAR(10) ,
	@EmailId VARCHAR(50),
	
	@Username VARCHAR(50),
	@Password VARCHAR(50),
	@UseType VARCHAR(50),
	
	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT	
)
AS 

	BEGIN

	--declaration
	DECLARE @ErrorMessage varchar(MAX)

		IF @Command='INSERT'
		BEGIN 

			BEGIN TRANSACTION
				BEGIN TRY 

				INSERT INTO tblPerson
				(
					FirstName,
					LastName,
					Type		
				)
				VALUES
				(
					@FirstName,
					@LastName,
					@Type				
				)

				SET @PersonId=@@IDENTITY

				IF @Type='CU'
					BEGIN

					EXEC uspSetCustomer @Command,@CustomerId,@PersonId,@HouseNo,@HouseName,@StreetName,
								@AreaName,@CityName,@StateName,@Pincode,@MobileNo,@EmailId,@Username,@Password,@UseType
					END
				ELSE
					BEGIN

					EXEC uspSetEmployee @Command,@EmployeeId,@PersonId,@HouseNo,@HouseName,@StreetName,
								@AreaName,@CityName,@StateName,@Pincode,@MobileNo,@EmailId,@Username,@Password,@UseType
					END

				SET @Status=1
					SET @Message='Insert succesfull'

					COMMIT TRANSACTION

				END TRY 

				BEGIN CATCH 
					SET @ErrorMessage=ERROR_MESSAGE()
				
						
					SET @Status=0
					SET @Message='Insert Exception'
					ROLLBACK TRANSACTION

					RAISERROR(@ErrorMessage,16,1)
				END CATCH	

		END

	END

			

