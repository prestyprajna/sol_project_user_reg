﻿CREATE PROCEDURE uspSetProduct
(
	@Command VARCHAR(50),
	@ProductID NUMERIC(18,0),
	@ProductName VARCHAR(50),
	@ProductPrice MONEY,
	@QtyAvailable INT,
	
	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT	
)
AS 

	BEGIN

	--declaration
	DECLARE @ErrorMessage varchar(MAX)


		IF @Command='INSERT'
		BEGIN 

		BEGIN TRANSACTION
			BEGIN TRY 

			INSERT INTO tblProduct
			(
				ProductName,
				ProductPrice,
				QtyAvailable
			)
			VALUES
			(
				@ProductName,
				@ProductPrice,
				@QtyAvailable
			)

				SET @Status=1
					SET @Message='Insert succesfull'

			COMMIT TRANSACTION

			END TRY 

			BEGIN CATCH 
				SET @ErrorMessage=ERROR_MESSAGE()

					SET @Status=0
					SET @Message='Insert Exception'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
			END CATCH	

		END


		


	END
