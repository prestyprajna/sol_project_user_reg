﻿CREATE PROCEDURE uspSetSalesTransaction
(
	@Command VARCHAR(50),
	@SalesTransactionId NUMERIC(18,0),
	@SalesTransactionDate DATETIME,
	@ProductId NUMERIC(18,0),
	@TotalQuantity INT,
	@TotalPrice BIGINT,
	@PersonId INT,
	@CurrentStatus VARCHAR(50),
	
	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT		
)
AS 

	BEGIN

	DECLARE @ErrorMessage Varchar(MAX)

		IF @Command='INSERT'
		BEGIN 

		BEGIN TRANSACTION

				BEGIN TRY 
			
			--declaration of variables
			DECLARE @tempTotalPrice	MONEY	
			DECLARE @tempQtyAvailable INT	
					

				--add transaction

				--SELECT @tempTotalPrice=P.ProductPrice
				--FROM tblProduct AS P
				--WHERE P.ProductId=@ProductId

				--SET @TotalPrice=@tempTotalPrice * @TotalQuantity

				INSERT INTO tblSalesTransaction
				(
					SalesTransactionDate,
					ProductId,
					TotalQuantity,
					TotalPrice,
					PersonId,
					Status										
				)
				VALUES
				(
					GETDATE(),
					@ProductId,
					@TotalQuantity,
					@TotalPrice,
					@PersonId,
					@CurrentStatus 
				) 

				--update total available qty
				SELECT @tempQtyAvailable=P.QtyAvailable 
				FROM tblProduct AS P
				WHERE P.ProductId=@ProductId

				SET @tempQtyAvailable=@tempQtyAvailable-@TotalQuantity

				UPDATE tblProduct
					SET QtyAvailable=@tempQtyAvailable
						WHERE ProductId=@ProductId	
						
				
				
			SET @Status=1
					SET @Message='Insert succesfull'

			COMMIT TRANSACTION

			END TRY 

			BEGIN CATCH 
				SET @ErrorMessage=ERROR_MESSAGE()

					SET @Status=0
					SET @Message='Insert Exception'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
			END CATCH				


		END

	END

