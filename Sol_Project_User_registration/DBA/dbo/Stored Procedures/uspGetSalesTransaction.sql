﻿CREATE PROCEDURE uspGetSalesTransaction
(
	@Command VARCHAR(50),
	@SalesTransactionId NUMERIC(18,0),
	@SalesTransactionDate DATETIME,
	@ProductId NUMERIC(18,0),
	@TotalQuantity INT,
	@TotalPrice BIGINT,
	@PersonId INT,
	@CurrentStatus VARCHAR(50),

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT
)
AS 

	BEGIN

	DECLARE @ErrorMessage Varchar(MAX)


		IF @Command='Select'
		BEGIN 
		
		BEGIN TRANSACTION

				BEGIN TRY 
			
			SELECT ST.SalesTransactionId,
			ST.SalesTransactionDate,
			ST.ProductId,
			ST.TotalQuantity,
			ST.TotalPrice,
			ST.PersonId,
			ST.Status,
			P.ProductPrice
				FROM tblSalesTransaction AS ST
					INNER JOIN
						tblProduct AS P
							ON ST.ProductId=P.ProductId
								WHERE ST.Status='PENDING'
					
				

						SET @Status=1
						SET @Message='Get Select Data'

					COMMIT TRANSACTION

				END TRY 
				
				BEGIN CATCH 
					
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Select Exception'

						RAISERROR(@ErrorMessage,16,1)

				END CATCH 

		END

		ELSE IF @Command='Select_By_Id_Status'
		BEGIN 
		
		BEGIN TRANSACTION

				BEGIN TRY 
			
			SELECT ST.SalesTransactionId,
			ST.SalesTransactionDate,
			ST.ProductId,
			ST.TotalQuantity,
			ST.TotalPrice,
			ST.PersonId,
			ST.Status,
			P.ProductPrice
				FROM tblSalesTransaction AS ST
					INNER JOIN
						tblProduct AS P
							ON ST.ProductId=P.ProductId
								WHERE ST.Status='PENDING' AND ST.ProductId=@ProductId
					
				

						SET @Status=1
						SET @Message='Get Select Data'

					COMMIT TRANSACTION

				END TRY 
				
				BEGIN CATCH 
					
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Select Exception'

						RAISERROR(@ErrorMessage,16,1)

				END CATCH 

		END


	END

