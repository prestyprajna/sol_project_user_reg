﻿CREATE PROCEDURE uspSetLogin
(
	@Command VARCHAR(50),
	@PersonId NUMERIC(18,0),	
	@Username VARCHAR(50),
	@Password VARCHAR(50),
	@UseType VARCHAR(50)
)
AS 

	BEGIN

	--declaration
	DECLARE @ErrorMessage varchar(MAX)

		IF @Command='INSERT'
		BEGIN 

		BEGIN TRANSACTION
			BEGIN TRY 

				INSERT INTO tblLogin
				(
					PersonId,
					Username,
					Password,
					UseType							
				)
				VALUES
				(
					@PersonId,
					@Username,
					@Password,
					@UseType
				) 
				COMMIT TRANSACTION

			END TRY 

			BEGIN CATCH 
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
			END CATCH	

		END


	END

