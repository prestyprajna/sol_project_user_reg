﻿CREATE PROCEDURE uspGetProduct
(
	@Command VARCHAR(50),
	@ProductID NUMERIC(18,0),
	@ProductName VARCHAR(50),
	@ProductPrice MONEY,
	@QtyAvailable INT,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT
)
AS 

	BEGIN

	DECLARE @ErrorMessage Varchar(MAX)

		IF @Command='GetById'
		BEGIN 

		BEGIN TRANSACTION

				BEGIN TRY 
			
			SELECT P.ProductId,
			P.ProductName,
			P.ProductPrice,
			P.QtyAvailable
				FROM tblProduct AS P
					WHERE ProductId=@ProductID

				SET @Status=1
						SET @Message='Get Data by Id Successful'

					COMMIT TRANSACTION

				END TRY 
				
				BEGIN CATCH 
					
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Get Data by Id exception'

						RAISERROR(@ErrorMessage,16,1)

				END CATCH 


		END


	END

