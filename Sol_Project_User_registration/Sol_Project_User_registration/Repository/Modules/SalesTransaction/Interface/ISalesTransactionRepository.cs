﻿using Sol_Project_User_registration.Common_Repository;
using Sol_Project_User_registration.Entity.Modules.SalesTransaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Repository.Modules.SalesTransaction.Interface
{
    public interface ISalesTransactionRepository: IInsert<SalesTransactionEntity>, ISelect<SalesTransactionEntity>
    {
    }
}
