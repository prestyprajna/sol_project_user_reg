﻿using Sol_Project_User_registration.Repository.Modules.SalesTransaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Project_User_registration.Entity.Modules.SalesTransaction;
using Sol_Project_User_registration.Concrete.Modules.SalesTransaction.Interface;
using Sol_Project_User_registration.Concrete.Modules.SalesTransaction;
using Sol_Project_User_registration.Entity.Modules.Product;
using Sol_Project_User_registration.ORM.Modules.SalesTransaction;

namespace Sol_Project_User_registration.Repository.Modules.SalesTransaction
{
    public class SalesTransactionRepository: ISalesTransactionRepository
    {        
        #region  declaration

        private ISalesTransactionConcrete salesTransactionConcreteObj = null;
        private ProductDC1DataContext dc = null;
        #endregion

        #region  construuctor

        public SalesTransactionRepository()
        {
            salesTransactionConcreteObj = new SalesTransactionConcrete();
            dc = new ProductDC1DataContext();
        }

        #endregion

        #region  public methods

        public async Task<bool> Insert(SalesTransactionEntity entityObj)
        {
            try
            {
                int? status = null;
                String message = null;

                await salesTransactionConcreteObj?.SetData(
                    "Insert",
                    entityObj,
                    (leStatus, leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    }
                    );

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {

                throw;
            }
            
        }
       
        public async Task<IEnumerable<SalesTransactionEntity>> Select(SalesTransactionEntity entityObj)
        {
            int? status = null;
            String message = null;

            try
            {
                var getQuery =
                    await salesTransactionConcreteObj?.GetData(
              "Select_By_Id_Status",
              entityObj,
              (leUspSalesTransactionObj) => new SalesTransactionEntity()
              {
                  SalesTransactionId = leUspSalesTransactionObj?.SalesTransactionId,
                  SalesTransactionDate = leUspSalesTransactionObj?.SalesTransactionDate,
                  ProductId = leUspSalesTransactionObj?.ProductId,
                  TotalQuantity = leUspSalesTransactionObj?.TotalQuantity,
                  //TotalPrice = ((long?) this.GetProductPriceData(entityObj).Result)  * (long?)leUspSalesTransactionObj?.TotalQuantity,
                  TotalPrice=leUspSalesTransactionObj?.TotalPrice,
                  PersonId = (int?)leUspSalesTransactionObj?.PersonId,
                  Status = leUspSalesTransactionObj?.Status,

              });
                return getQuery;
            }
            catch (Exception)
            {

                throw;
            }

        }

        #endregion

        #region  public method to get product data or product price

        public async Task<dynamic> GetProductPriceData(SalesTransactionEntity salesTransactionEntityObj)
        {
            return await Task.Run(() =>
            {
                var getQuery =
                dc?.tblProducts
                ?.AsEnumerable()
                ?.Where((leTblProductObj) => leTblProductObj.ProductId == salesTransactionEntityObj.ProductId)               
                ?.Select((letblObj) => new ProductEntity()
                {
                    ProductPrice = letblObj?.ProductPrice
                })
                ?.FirstOrDefault()
                ?.ProductPrice;
               

                return getQuery;
            });

        }

        #endregion
    }
}
