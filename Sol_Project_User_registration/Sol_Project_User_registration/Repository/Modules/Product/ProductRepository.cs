﻿using Sol_Project_User_registration.Repository.Modules.Product.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Project_User_registration.Entity.Modules.Product;
using Sol_Project_User_registration.Concrete.Modules.Product.Interface;
using Sol_Project_User_registration.Concrete.Modules.Product;

namespace Sol_Project_User_registration.Repository.Modules.Product
{
    public class ProductRepository : IProductRepository
    {
        #region  declaration

        private IProductConcrete productConcreteObj = null;

        #endregion

        #region  constructor

        public ProductRepository()
        {
            productConcreteObj = new ProductConcrete();
        }

        #endregion

        #region  public methods

        public async Task<bool> Insert(ProductEntity entityObj)
        {
            try
            {

                int? status = null;
                String message = null;

                await productConcreteObj?.SetData(
                "Insert",
                entityObj,
                (leStatus, leMessage) =>
                {
                    status = leStatus;
                    message = leMessage;
                }
                );

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

    }
}
