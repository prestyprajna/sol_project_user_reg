﻿using Sol_Project_User_registration.Common_Repository;
using Sol_Project_User_registration.Entity.Modules.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Repository.Modules.Product.Interface
{
    public interface IProductRepository: IInsert<ProductEntity>
    {
    }
}
