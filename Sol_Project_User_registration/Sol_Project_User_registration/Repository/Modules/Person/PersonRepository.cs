﻿using Sol_Project_User_registration.Repository.Modules.Person.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Project_User_registration.Entity.Modules.Person;
using Sol_Project_User_registration.Concrete.Modules.Person.Interface;
using Sol_Project_User_registration.Concrete.Modules.Person;

namespace Sol_Project_User_registration.Repository.Modules.Person
{
    public class PersonRepository : IPersonRepository
    {
        #region  declaration

        private IPersonConcrete personConcreteObj = null;

        #endregion

        #region  constructor

        public PersonRepository()
        {
            personConcreteObj = new PersonConcrete();
        }

        #endregion

        #region  public methods

        public async Task<bool> Insert(PersonEntity entityObj)
        {
            try
            {
                int? status = null;
                String message = null;

                await personConcreteObj.SetData(
                    "Insert",
                    entityObj,
                    (leStatus, leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    }
                    );

                return (status == 1) ? true : false;

            }
            catch (Exception)
            {

                throw;
            }
           
        }

        #endregion



    }
}
