﻿using Sol_Project_User_registration.Common_Repository;
using Sol_Project_User_registration.Entity.Modules.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Repository.Modules.Person.Interface
{
    public interface IPersonRepository: IInsert<PersonEntity>
    {
    }
}
