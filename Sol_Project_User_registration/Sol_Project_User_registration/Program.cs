﻿using Sol_Project_User_registration.Entity.Modules.Person;
using Sol_Project_User_registration.Entity.Modules.Product;
using Sol_Project_User_registration.Entity.Modules.SalesTransaction;
using Sol_Project_User_registration.Repository.Modules.Person;
using Sol_Project_User_registration.Repository.Modules.Person.Interface;
using Sol_Project_User_registration.Repository.Modules.Product;
using Sol_Project_User_registration.Repository.Modules.Product.Interface;
using Sol_Project_User_registration.Repository.Modules.SalesTransaction;
using Sol_Project_User_registration.Repository.Modules.SalesTransaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                try
                {
                    IPersonRepository personRepositoryObj = new PersonRepository();

                    IProductRepository productRepositoryObj = new ProductRepository();

                    ISalesTransactionRepository salesTransactionRepositoryObj = new SalesTransactionRepository();

                    #region customer data
                    //var flag = await personRepositoryObj.Insert(new PersonEntity()
                    //{
                    //    FirstName = "PRESTY",
                    //    LastName = "PRAJNA",
                    //    Type = "CU",
                    //    addressEntityObj = new PersonAddressEntity()
                    //    {
                    //        HouseNo = "B/9",
                    //        HouseName = "SAIBABA SOCIETY",
                    //        StreetName = "N.S.S ROAD",
                    //        AreaName = "ASALPHA VILLAGE",
                    //        CityName = "GHATKOPAR",
                    //        StateName = "MUMBAI",
                    //        Pincode = 400084

                    //    },
                    //    communicationEntityObj = new PersonCommunicationEntity()
                    //    {
                    //        MobileNo = "8108940725",
                    //        EmailId = "presty@gmail.com"
                    //    },
                    //    loginEntityObj = new PersonLoginEntity()
                    //    {
                    //        Username = "PRESTY123",
                    //        Password = "123",
                    //        UseType = "CUSTOMER"
                    //    }

                    //});

                    #endregion

                    #region  employee1 data

                    //var flag1 = await personRepositoryObj.Insert(new PersonEntity()
                    //{
                    //    FirstName = "DEEPAK",
                    //    LastName = "KOTIAN",
                    //    Type = "EM",
                    //    addressEntityObj = new PersonAddressEntity()
                    //    {
                    //        HouseNo = "B/19",
                    //        HouseName = "SAIBABA SOCIETY",
                    //        StreetName = "N.S.S ROAD",
                    //        AreaName = "ASALPHA VILLAGE",
                    //        CityName = "GHATKOPAR",
                    //        StateName = "MUMBAI",
                    //        Pincode = 400084

                    //    },
                    //    communicationEntityObj = new PersonCommunicationEntity()
                    //    {
                    //        MobileNo = "8108990725",
                    //        EmailId = "deepak@gmail.com"
                    //    },
                    //    loginEntityObj = new PersonLoginEntity()
                    //    {
                    //        Username = "DEEPAK",
                    //        Password = "345",
                    //        UseType = "SALES"
                    //    }

                    //});

                    #endregion

                    #region  employee2 data

                    //var flag2 = await personRepositoryObj.Insert(new PersonEntity()
                    //{
                    //    FirstName = "RITURAJ",
                    //    LastName = "KOTIAN",
                    //    Type = "EM",
                    //    addressEntityObj = new PersonAddressEntity()
                    //    {
                    //        HouseNo = "B/10",
                    //        HouseName = "SAIBABA SOCIETY",
                    //        StreetName = "N.S.S ROAD",
                    //        AreaName = "ASALPHA VILLAGE",
                    //        CityName = "GHATKOPAR",
                    //        StateName = "MUMBAI",
                    //        Pincode = 400084

                    //    },
                    //    communicationEntityObj = new PersonCommunicationEntity()
                    //    {
                    //        MobileNo = "8208990725",
                    //        EmailId = "rituraj@gmail.com"
                    //    },
                    //    loginEntityObj = new PersonLoginEntity()
                    //    {
                    //        Username = "RITURAJ",
                    //        Password = "395",
                    //        UseType = "ADMIN"
                    //    }

                    //});

                    #endregion 

                    #region  PRODUCT DATA INSERT

                    //var flag3 = await productRepositoryObj.Insert(new ProductEntity()
                    //{
                    //    ProductName = "MILK POWDER",
                    //    ProductPrice = 60,
                    //    QtyAvailable = 40
                    //});

                    #endregion

                    #region  SALES TRANSACTION INSERT DATA

                    //var flag4 = await salesTransactionRepositoryObj?.Insert(new SalesTransactionEntity()
                    //{
                    //    SalesTransactionDate = DateTime.Now,
                    //    ProductId = 4,
                    //    TotalQuantity = 2,
                    //    //TotalPrice=null,
                    //    PersonId = 1,
                    //    Status = "PENDING"

                    //});

                    #endregion

                    #region  SALES TRANSACTION SELECT DATA

                    IEnumerable<SalesTransactionEntity> salesTransactionEntityObj = await salesTransactionRepositoryObj?.Select(new SalesTransactionEntity()
                    {
                        ProductId = 4,
                        Status = "PENDING"
                    });

                    foreach (SalesTransactionEntity val in salesTransactionEntityObj)
                    {
                        Console.WriteLine(val.SalesTransactionId);
                        Console.WriteLine(val.SalesTransactionDate);
                        Console.WriteLine(val.ProductId);
                        Console.WriteLine(val.TotalQuantity);
                        Console.WriteLine(val.TotalPrice);
                        Console.WriteLine(val.PersonId);
                        Console.WriteLine(val.Status);
                    }

                    #endregion

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }
            ).Wait();

        }
    }
}
