﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Common_Repository
{
    public interface ISetDelegate<TEntity> where TEntity : class
    {
        Task<dynamic> SetData(String Command, TEntity entityObj, Action<int?, String> storedProcedureOutPara = null);
    }

}
