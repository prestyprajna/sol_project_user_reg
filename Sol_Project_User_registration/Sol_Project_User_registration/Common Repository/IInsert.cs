﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Common_Repository
{
    public interface IInsert<TEntity> where TEntity : class
    {
        Task<Boolean> Insert(TEntity entityObj);
    }  
}
