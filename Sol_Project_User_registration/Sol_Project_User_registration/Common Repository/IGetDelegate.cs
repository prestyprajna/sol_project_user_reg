﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Common_Repository
{
    public interface IGetDelegate<TEntity, TEntityResultSet>
        where TEntity : class where TEntityResultSet : class
    {
        Task<dynamic> GetData(
            String command,
            TEntity entityObj,
            Func<TEntityResultSet, TEntity> selector,
            Action<int?, String> storedProcedureOutPara = null
            );
    }
}
