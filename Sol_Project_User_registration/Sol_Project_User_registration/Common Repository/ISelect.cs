﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Common_Repository
{
    public interface ISelect<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> Select(TEntity entityObj);
    }
}
