﻿using Sol_Project_User_registration.Concrete.Modules.Person.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Project_User_registration.Entity.Modules.Person;
using Sol_Project_User_registration.ORM.Modules.Person;

namespace Sol_Project_User_registration.Concrete.Modules.Person
{
    public class PersonConcrete : IPersonConcrete
    {
        #region  declaration

        private PersonDCDataContext dc = null;

        #endregion

        #region  constructor

        public PersonConcrete()
        {
            dc = new PersonDCDataContext();
        }

        #endregion

        #region  public methods
        public async Task<dynamic> SetData(string Command, PersonEntity personEntityObj, Action<int?, string> storedProcedureOutPara = null)
        {
            int? status = null;
            String message = null;

            try
            {
                return await Task.Run(() =>
                {
                    var setQuery =
                    dc
                    ?.uspSetPerson(
                        Command,
                    personEntityObj?.PersonId,
                    personEntityObj?.customerEntityObj?.CustomerId,
                    personEntityObj?.employeeEntityObj?.EmployeeId,
                    personEntityObj?.FirstName,
                    personEntityObj?.LastName,
                    personEntityObj?.Type,
                    personEntityObj?.addressEntityObj?.HouseNo,
                    personEntityObj?.addressEntityObj?.HouseName,
                    personEntityObj?.addressEntityObj?.StreetName,
                    personEntityObj?.addressEntityObj?.AreaName,
                    personEntityObj?.addressEntityObj?.CityName,
                    personEntityObj?.addressEntityObj?.StateName,
                    personEntityObj?.addressEntityObj?.Pincode,
                    personEntityObj?.communicationEntityObj?.MobileNo,
                    personEntityObj?.communicationEntityObj?.EmailId,
                    personEntityObj?.loginEntityObj?.Username,
                    personEntityObj?.loginEntityObj?.Password,
                    personEntityObj?.loginEntityObj?.UseType,
                    ref status,
                    ref message

                        );

                    storedProcedureOutPara(status, message);

                    return setQuery;
                });
            }
            catch (Exception)
            {

                throw;
            }

            
        }
        #endregion
    }
}
    
