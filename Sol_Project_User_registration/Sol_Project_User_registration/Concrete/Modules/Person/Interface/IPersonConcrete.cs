﻿using Sol_Project_User_registration.Entity.Modules.Person;
using Sol_Project_User_registration.Common_Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Concrete.Modules.Person.Interface
{
    public interface IPersonConcrete: ISetDelegate<PersonEntity>
    {

    }
}
