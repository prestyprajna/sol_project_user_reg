﻿using Sol_Project_User_registration.Concrete.Modules.Product.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Project_User_registration.Entity.Modules.Product;
using Sol_Project_User_registration.ORM.Modules.Product;

namespace Sol_Project_User_registration.Concrete.Modules.Product
{
    public class ProductConcrete : IProductConcrete
    {
        #region Delcaration

        private ProductDCDataContext dc = null;

        #endregion

        #region Constructor

        public ProductConcrete()
        {
            dc = new ProductDCDataContext();
        }

        #endregion


        #region  public methods
        public async Task<dynamic> SetData(string Command, ProductEntity entityObj, Action<int?, string> storedProcedureOutPara = null)
        {
            try
            {
                int? status = null;
                string message = null;
                return await Task.Run(() =>
                {
                    var setQuery =
                    dc?.uspSetProduct(
                        Command,
                        entityObj?.ProductId,
                        entityObj?.ProductName,
                        entityObj?.ProductPrice,
                        entityObj?.QtyAvailable,
                        ref status,
                        ref message
                        );

                    storedProcedureOutPara(status, message);

                    return setQuery;
                });
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
