﻿using Sol_Project_User_registration.Concrete.Modules.SalesTransaction.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Project_User_registration.Entity.Modules.SalesTransaction;
using Sol_Project_User_registration.ORM.Modules.SalesTransaction;
using Sol_Project_User_registration.Repository.Modules.SalesTransaction;

namespace Sol_Project_User_registration.Concrete.Modules.SalesTransaction
{
    public class SalesTransactionConcrete : ISalesTransactionConcrete
    {
        #region  declaration

        private SalesTransactionDC1DataContext dc = null;

        #endregion

        #region  construuctor

        public SalesTransactionConcrete()
        {
            dc = new SalesTransactionDC1DataContext();
        }
       
        #endregion

        #region  public methods

        public async Task<dynamic> SetData(string Command, SalesTransactionEntity entityObj, Action<int?, string> storedProcedureOutPara = null)
        {
            try
            {
                return await Task.Run(() =>
                {
                    int? status = null;
                    string message = null;

                    var setQuery =
                    dc?.uspSetSalesTransaction(
                        Command,
                        entityObj?.SalesTransactionId,
                        entityObj?.SalesTransactionDate,
                        entityObj?.ProductId,
                        entityObj?.TotalQuantity,
                        entityObj.TotalPrice=
                        (long?)new SalesTransactionRepository().GetProductPriceData(entityObj).Result*entityObj?.TotalQuantity,
                        entityObj?.PersonId,
                        entityObj?.Status,
                        ref status,
                        ref message
                        );

                    storedProcedureOutPara(status, message);

                    return setQuery;
                });
            }
            catch (Exception)
            {

                throw;
            }
           
        }


        public async Task<dynamic> GetData(string command, SalesTransactionEntity entityObj, Func<uspGetSalesTransactonResultSet, SalesTransactionEntity> selector, Action<int?, string> storedProcedureOutPara = null)
        {
            try
            {
                int? status = null;
                String message = null;

                return await Task.Run(() =>
                {
                    var getQuery =
                    dc?.uspGetSalesTransaction(
                        command,
                            entityObj?.SalesTransactionId,
                            entityObj?.SalesTransactionDate,
                            entityObj?.ProductId,
                            entityObj?.TotalQuantity,
                            entityObj?.TotalPrice,
                            entityObj?.PersonId,
                            entityObj?.Status,
                            ref status,
                            ref message
                        )
                        .AsEnumerable()
                        .Select((leEntity) => selector(leEntity))
                                .ToList();

                    return getQuery;


                });
            }
            catch (Exception)
            {

                throw;
            }
           
        }

       

        #endregion

    }
}
