﻿using Sol_Project_User_registration.Common_Repository;
using Sol_Project_User_registration.Entity.Modules.SalesTransaction;
using Sol_Project_User_registration.ORM.Modules.SalesTransaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Concrete.Modules.SalesTransaction.Interface
{
    public interface ISalesTransactionConcrete: ISetDelegate<SalesTransactionEntity>, IGetDelegate<SalesTransactionEntity, uspGetSalesTransactonResultSet>
    {
    }
}
