﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Entity.Modules.Product
{
    public class ProductEntity
    {
        public decimal? ProductId { get; set; }

        public string ProductName { get; set; }

        public decimal? ProductPrice { get; set; }

        public int QtyAvailable { get; set; }

    }
}
