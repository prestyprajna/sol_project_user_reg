﻿using Sol_Project_User_registration.Entity.Modules.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Entity.Modules.SalesTransaction
{
    public class SalesTransactionEntity
    {
        public decimal? SalesTransactionId { get; set; }

        public DateTime? SalesTransactionDate { get; set; }

        public decimal? ProductId { get; set; }

        public int? TotalQuantity { get; set; }

        public long? TotalPrice { get; set; }

        public int?  PersonId { get; set; }

        public string Status { get; set; }

        //public ProductEntity productEntityObj { get; set; }

    }
}
