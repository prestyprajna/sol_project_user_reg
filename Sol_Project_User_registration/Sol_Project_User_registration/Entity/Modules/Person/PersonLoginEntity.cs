﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Entity.Modules.Person
{
    public class PersonLoginEntity
    {
        public decimal PersonId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string UseType { get; set; }
    }
}
