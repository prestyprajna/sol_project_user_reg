﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Entity.Modules.Person
{
    public class PersonAddressEntity
    {
        public decimal PersonId { get; set; }

        public string HouseNo { get; set; }

        public string HouseName { get; set; }

        public string StreetName { get; set; }

        public string AreaName { get; set; }

        public string CityName { get; set; }

        public string StateName { get; set; }

        public long Pincode { get; set; }
    }
}
