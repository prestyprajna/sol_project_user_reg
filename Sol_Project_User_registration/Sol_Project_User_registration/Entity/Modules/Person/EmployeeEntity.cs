﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Entity.Modules.Person
{
    public class EmployeeEntity
    {
        public decimal EmployeeId { get; set; }

        public decimal PersonId { get; set; }

        public PersonAddressEntity addressEntityObj { get; set; }

        public PersonCommunicationEntity communicationEntityObj { get; set; }

        public PersonLoginEntity loginEntityObj { get; set; }
    }
}
