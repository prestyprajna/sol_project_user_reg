﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Entity.Modules.Person
{
    public class PersonEntity
    {
        public decimal PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Type { get; set; }

        public CustomerEntity customerEntityObj { get; set; }

        public EmployeeEntity employeeEntityObj { get; set; }

        public PersonAddressEntity addressEntityObj { get; set; }

        public PersonCommunicationEntity communicationEntityObj { get; set; }

        public PersonLoginEntity loginEntityObj { get; set; }
    } 
}
