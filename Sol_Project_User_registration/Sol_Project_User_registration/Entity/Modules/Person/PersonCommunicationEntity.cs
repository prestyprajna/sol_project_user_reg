﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Project_User_registration.Entity.Modules.Person
{
    public class PersonCommunicationEntity
    {
        public decimal PersonId { get; set; }

        public string MobileNo { get; set; }

        public string EmailId { get; set; }
    }
}
